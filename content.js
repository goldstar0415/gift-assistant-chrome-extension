function isWebsiteUnknown(website) {
    websiteList = getWebsiteList();
    var result = true;
    var regexp;
    for (var i = 0; i < websiteList.length; i++) {
        test = new RegExp(websiteList[i]);
        if (test.exec(website)) result = false;
    }
    return result;
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.action == "getItem") {
        var website = location.hostname.toLowerCase();
        sendResponse({"itemData": getItem(website), "unknownWebsiteData": isWebsiteUnknown(website)});
    }
    else{
        console.log('error');
    }
});

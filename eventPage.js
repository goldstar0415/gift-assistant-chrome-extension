var isChromium = window.chrome,
    winNav = window.navigator,
    vendorName = winNav.vendor;
if (isChromium !== null && typeof isChromium !== "undefined" && vendorName === "Google Inc.") { //if Chrome
    
    chrome.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            if (request.message) {
                if (request.message == "logged_in") {
                    sendResponse({logged_in: 'true'});
                }
            }
            if (request.token == "not added") {
                window.token_string = "";
                localStorage.setItem("giftibly_token", "");
            }
            else{
                window.token_string = request.token;
                localStorage.setItem("giftibly_token", token_string);
            }
            return true;
        }
    );
    
     chrome.extension.onConnect.addListener(function(port) {
          port.onMessage.addListener(function(msg) {
               console.log(msg);
               if(msg == "token not added"){
                   port.postMessage(token_string);
               }
          });
     });
}
else{
     browser.runtime.onMessage.addListener(
        function(request, sender, sendResponse) {
            if (request.message) {
                if (request.message == "logged_in") {
                    sendResponse({logged_in: 'true'});
                }
            }
            if (request.token == "not added") {
                window.token_string = "";
                localStorage.setItem("giftibly_token", "");
            }
            else{
                window.token_string = request.token;
                localStorage.setItem("giftibly_token", token_string);
            }
            return true;
        }
     );
     browser.runtime.onConnect.addListener(function(port) {
          port.onMessage.addListener(function(msg) {
               console.log(msg);
               if(msg == "token not added"){
                   port.postMessage(token_string);
               }
          });
     });
}
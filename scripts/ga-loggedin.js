    var isChromium = window.chrome,
        winNav = window.navigator,
        vendorName = winNav.vendor;

    if ((isChromium !== null && typeof isChromium !== "undefined" && vendorName === "Google Inc.") || (navigator.userAgent.toLowerCase().indexOf('firefox') > -1)) {  //detect if Chrome or Firefox is the Browser
        if($('#ga_logged_in')[0]){
            var token_string = $('#ga_logged_in').data('token');
            chrome.runtime.sendMessage({ message: "logged_in",  token: token_string},
                function (reply) {
                    if (reply.logged_in == "true") {
                        localStorage.setItem("giftibly_token", "added");
                    }
                }
            );
        }
        else if($('#ga_logged_out')[0]){
            var token_string = 'not added';
            chrome.runtime.sendMessage({token: token_string});
            localStorage.setItem("giftibly_token", "");
        }
    }

$(document).ready(function() {
    $('#gift_assistant_link').remove();
    $('.ga_button').remove();
});

$(document).on('turbolinks:load', function() {
    $('#gift_assistant_link').remove();
    $('.ga_button').remove();
});